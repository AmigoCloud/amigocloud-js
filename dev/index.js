import settingsProvider from '../src/lib/settings'
import SocketIOWrapper from '../src/lib/SocketIOWrapper'

import {
  BASE_URL,
  USER_ID,
  ACCESS_TOKEN
} from './constants'

const updateCallback = status => {
  console.log(status)
  const statusElement = document.getElementById('status')
  const statusStringElement = document.getElementById('status-string')

  statusElement.className = status
  statusStringElement.innerText = status
}

settingsProvider.set({
  baseUrl: BASE_URL,
  userId: USER_ID,
  accessToken: ACCESS_TOKEN
})

const socketIO = new SocketIOWrapper()

socketIO.listenOn(['dataset:change_succeeded', 'project:preview_image_updated'])
  .subscribe({
    next: (data) => {
      console.log('subs', data)
    }
  })

socketIO.statusObservable.subscribe({
  next: (status) => {
    updateCallback(status)
  }
})
