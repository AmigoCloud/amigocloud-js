# How to use the `@amigocloud/amigocloud` library

The `amigocloud-js` library provides you with the ability to connect to our `socket.io` backend server, to do so you will need to instal the library using yarn:

```bash
yarn add @amigocloud/amigocloud
```

After installing the library, you can import it into your code and start interacting with the amigocloud `socket.io` backend service.

```js
import {
  settingsProvider,
  SocketIOWrapper
} from '@amigocloud/amigocloud'

// you need to provide the userId and an accessToken so the library knows where
// to connect, you can also provide a custom baseUrl parameter in case you're
// connecting to another amigocloud deployment
settingsProvider.set({
  baseUrl: 'https://app.amigocloud.com/', // optional parameter
  userId: 1,
  accessToken: ''
})

const socketIO = new SocketIOWrapper()

// start listening to the socket.io events you want to, you can add as many as
// you want, or have separate listeners according to your needs.
const eventsListener = socketIO.listenOn([
  'dataset:change_succeeded',
  'project:preview_image_updated'
])
  .subscribe({
    next: (data) => {
      // TODO: show the data object structure so the user knows how to filter events
      console.log('new event data received', data)
    }
  })

// in case you want to monitor the status of your connection with the socket.io
// service, you can subscribe to the statusObservable object.
const statusListener = socketIO.statusObservable.subscribe({
  next: (status) => {
    console.log(`Status updated: ${status}`)
  }
})
```

The regular event object will look something like this:

```json
{
  "eventName": "project:preview_image_updated",
  "eventData": {
    "project_id": 34,
    "id": "513eaa1d3c084ab3848ef6f330e2e5fa",
    "extra": {
      "cache_busting": "2494289ad2d54c89a046e9ddd9709f17"
    }
  },
  "fromJob": false
}
```

Based on the `eventName` and `eventData` you should be able to make your application behave as you desire.

When you are done using `@amigocloud/amigocloud`, don't forget to unsubscribe your active subscriptions to the events, this usually will be done in a `componentWillUnmount` or whenever you're destroying the view that relies on the library to update an element.

```js
eventsListener.unsubscribe()
statusListener.unsubscribe()
```

# Documentation

Documentation can be found [here](https://amigocloud.gitlab.io/amigocloud-js/).
