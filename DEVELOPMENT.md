# How to develop and improve amigocloud-js

## Requirements

The following tools are necessary to start working on adding new features to the `@amigocloud/amigocloud` library

- `node` version 11.11.0 at least
- `yarn`

If you have another version of `node`, I would recomend to use [nvm](https://github.com/nvm-sh/nvm) so you can have multiple version of `node` running.

## Install

For development, you need to install dependencies with the command:

```
yarn
```

## Local Development

For local development `yarn dev`, this will spawn a local development server at `http://localhost:1234/` from where you can test a working application that uses `amigocloud-js`:

```
yarn dev
```

To run the tests, use:

```
yarn test
```

## Commiting changes

When changes are ready, commit your changes as usual with `git`, don't forget to include the `package.json`, as well as, the `yarn.lock` file, which is really important when you add, remove or upgrade packages, as it holds all the dependencies information and the exact version of the libraries being used.

Tools like `husky` and `standardjs` will take care of reviewing and formatting the code as much as possible, there are cases that standardjs will complain about malformed code or not good code being used, so you will have to take a look and fix them manually. For more information on how they are configured check the `package.json` file.

## Deploying

Before deploying the code to npm, make sure all the changes are commited.

An access token to gitlab is required so releases can be created when the `yarn deploy` command is run, if you already have one, make sure it is available as an evironment variable with the name `GITLAB_TOKEN`, if you don't have an access token, go to https://gitlab.com/-/profile/personal_access_tokens and create an access token with at least `api` scope.

Also, if you haven't log in into the npm registry, you will have to do so using the amigocloud credentials that will be provided by one of the developers:

```
npm login
```

It will ask for the username, password and email associated with the amigocloud account.

After all changes are merged and all the fixes have been done, to be able to deploy a new version of the library, you will have to use the following command:

```
yarn deploy
```

This command will take care of:
- Running the tests.
- Asking the user to setup the next version of the package.
- Pushing the code to gitlab.
- Creating the corresponding tag and release.
- Update the `CHANGELOG.md` file.
- And finally pushing the code to the npm registry.

It is an interactive prompt so you will have to take care of answering all the questions provided.

Finally, to update the dependency version in our amigocloud application, you will have to run `yarn upgrade @amigocloud/amigocloud@<corresponding version>`.

# Documentation

Documentation will be generated every time an update to master us pushed, which is exactly what happens when `yarn deploy` is run, the generated documentation can be found [here](https://amigocloud.gitlab.io/amigocloud-js/).
