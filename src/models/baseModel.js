import { Observable, fromEvent, of } from 'rxjs'
import { filter } from 'rxjs/operators'
import requestWorker from '../workers/requestWorkerClient'

class BaseModel {
  constructor (id = '') {
    this.observable = fromEvent(requestWorker, 'message')
    this.cachedData = null
    this.id = id
  }

  _cleanUpItem = item => {
    return JSON.parse(JSON.stringify(item))
  }

  _addHeaders = () => {
    // to be implemented in client class
    return {}
  }

  // generates id for comunication with web requestWorker
  getModelId = () => {
    console.warn('should be implented in Child class')
  }

  // transforms data in a meaning way for the model
  transformData = data => {
    console.warn('should be implented in Child class')
  }

  // stores data in cache
  storeData = data => {
    this.cachedData = this.transformData(data)
  }

  // gets stored data in cache
  getCache = () => {
    return this._cleanUpItem(this.cachedData)
  }

  // clears cache
  clearCache = () => {
    this.cachedData = null
  }

  // returns true in case cache is empty
  isCacheEmpty = () => {
    return !this.cachedData
  }

  sendRequest = ({
    method,
    url = null,
    refresh = false,
    params = undefined,
    data = undefined,
    useForm = false,
    extra = {}
  }) => {
    if (!this.isCacheEmpty() && !refresh) {
      // return data from cache
      return of(this.getCache())
    } else {
      // request data from API
      // creates an observable from the function given
      return new Observable(observer => {
        // subscribe to observable
        const subscription = this.observable
          .pipe(filter(response => response.data.id === this.getModelId()))
          .subscribe(response => {
            // clear cache
            this.clearCache()

            if (response.data.error) {
              // in case of error, return the error back
              observer.error(response.data.error)
            } else {
              // store data in cache
              this.storeData(response.data)

              // sends response to subscriptor
              observer.next(this.getCache())

              // complete observer
              observer.complete()
            }

            // unsubcribe to avoid repeated events
            subscription.unsubscribe()
          })

        // send message to requestWorker so it retreives data from API
        requestWorker.postMessage({
          id: this.getModelId(),
          url: url || this.source,
          method,
          params,
          data,
          useForm,
          ...this._addHeaders(method),
          ...extra
        })
      })
    }
  }

  getModelData = ({ refresh = false, params }) => {
    return this.sendRequest({
      method: 'get',
      refresh,
      params
    })
  }

  postModelData = (data, useForm = false) => {
    return this.sendRequest({
      method: 'post',
      refresh: true,
      data,
      useForm
    })
  }

  putModelData = data => {
    return this.sendRequest({
      method: 'put',
      refresh: true,
      data
    })
  }

  patchModelData = (data, useForm = false) => {
    return this.sendRequest({
      method: 'patch',
      refresh: true,
      data,
      useForm
    })
  }

  delete = url => {
    return this.sendRequest({
      url,
      method: 'delete',
      refresh: true
    })
  }

  deleteModelData = data => {
    return this.sendRequest({
      method: 'delete',
      refresh: true,
      data
    })
  }
}

export default BaseModel
