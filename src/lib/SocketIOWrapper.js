import io from 'socket.io-client'
import ioWildcard from 'socketio-wildcard'
import qs from 'qs'
import axios from 'axios'
import axiosRetry from 'axios-retry'
import { Subject } from 'rxjs'

import settings from './settings'
import SocketIOModel from '../models/socketIOWrapperModel'
import {
  SOCKET_IO_SESSION_URL,
  SOCKET_IO_SETTINGS,
  SOCKET_IO_CONNECTED,
  SOCKET_IO_AUTHENTICATING,
  SOCKET_IO_DISCONNECTED,
  SOCKET_IO_RECONNECTING,
  SOCKET_IO_NO_CONNECTION
} from '../constants/socketIOConstants'
import { MAX_REQUESTS_RETRIES } from '../constants'

const retryCondition = error => {
  return (
    axiosRetry.isNetworkOrIdempotentRequestError(error) ||
    // in case we get a network timeout it should also retry
    error.code === 'ECONNABORTED'
  )
}

function AmigoError (name, message) {
  this.name = name
  this.message = message
}

/**
 * Class wrapper aroud SocketIO which handles authentication and reconection
 * with the amigocloud socket.io backend server
 * @constructor
 * @returns {Object} SocketIOWrapperInstance
 */
class SocketIOWrapper {
  socketIO = null
  socketIOStatusUpdateCallbacks = []

  constructor () {
    const { baseUrl } = settings.get()

    // check if it already exists to mantain single instance
    if (typeof SocketIOWrapper.instance !== 'undefined') {
      return SocketIOWrapper.instance
    }

    // check for malformed url
    if (baseUrl && !baseUrl.endsWith('/')) {
      throw new AmigoError(
        'MalformedUrl',
        'Custom host provided needs to end with "/"'
      )
    }

    // custom axios with retry
    this.axiosClient = axios.create({ baseURL: baseUrl })
    axiosRetry(this.axiosClient, {
      retries: MAX_REQUESTS_RETRIES,
      retryDelay: axiosRetry.exponentialDelay,
      retryCondition
    })

    const {
      server,
      path,
      transports,
      reconnectionDelay,
      randomizationFactor
    } = SOCKET_IO_SETTINGS

    // init socket.io instance
    this.socketIO = io.connect(`${baseUrl || '/'}${server}`, {
      path,
      transports,
      reconnectionDelay,
      randomizationFactor
    })

    // io wildcard allow us to have a single entry point for socket.io on method
    // this will be helpfull in the rxjs part
    const patchSocketIO = ioWildcard(io.Manager)
    patchSocketIO(this.socketIO)

    // socket.io internal events handling
    this.socketIO.on('connect', this._authenticate)
    this.socketIO.on('reconnect', this._authenticate)
    this.socketIO.on('disconnect', this._onDisconnect)
    this.socketIO.on('disconnecting', this._onError)
    this.socketIO.on('connect_error', this._onError)

    /**
     * @private
     * RxJs Model in charge of emiting a new state every time a socket.io event
     * is received by the js code. It will act as a bridge when the user call
     * the `listerOn` method.
     */
    this.socketIOModelInstance = new SocketIOModel(this.socketIO)
    /**
     * RxJs Observable which will emit new states anytime the connection status
     * with the backend changes
     */
    this.statusObservable = new Subject()

    SocketIOWrapper.instance = this
    return this
  }

  /**
   * @method
   * @param {Array<String>} eventsList - Array of event names you want to listen
   * @param {Object?} takeUntilObservable - observable that will indicate until when we want to listen to the events in eventsList
   * @returns {Object} observable - RxJs observable to which a client can subscribe to receive updates
   */
  listenOn = (eventsList, takeUntilObservable) => {
    return this.socketIOModelInstance.listenOn(eventsList, takeUntilObservable)
  }

  /**
   * Wrapper around internal `_authenticate` method, it is useful as it lets you set the userId inside the SettingsProvider service
   * @method
   * @param {integer} userId - User id from the user we want to authenticate
   * @param {object} extra - Object with extra attributes we want to pass to the authentication process
   * @returns {void}
   */
  authenticate = (userId, extra = {}) => {
    settings.set({
      userId
    })

    return this._authenticate(extra)
  }

  _updateStatus = status => {
    this.statusObservable.next(status)
  }

  _buildUrl = (endPoint, params = {}) => {
    const qsSettings = {
      addQueryPrefix: true
    }
    const stringifiedParams = qs.stringify(params, qsSettings)

    return `${endPoint}${stringifiedParams}`
  }

  _onDisconnect = () => {
    this._updateStatus(SOCKET_IO_RECONNECTING)
  }

  _onError = () => {
    this._updateStatus(SOCKET_IO_NO_CONNECTION)
  }

  /**
   * Will hit the `/api/v1/me/start_websocket_session` endpoint to obtain the
   * `websocket_session` token so it can authenticate with the socket.io backend
   * @method
   * @param {Object} extra - Aditional parameters that may want to be sent to authenticate with the backend
   * @returns {void}
   */
  _authenticate = (extra = {}) => {
    const { userId } = settings.get()

    if (!userId) {
      return
    }

    const { accessToken } = settings.get()
    const url = this._buildUrl(SOCKET_IO_SESSION_URL, {
      token: accessToken
    })

    this._updateStatus(SOCKET_IO_AUTHENTICATING)

    this.axiosClient
      .get(url)
      .then(({ data: { websocket_session: websocketSession } }) => {
        const data = {
          userid: userId,
          websocket_session: websocketSession,
          ...extra
        }

        this.socketIO.emit('authenticate', data, connected => {
          if (connected) {
            this._updateStatus(SOCKET_IO_CONNECTED)
          } else {
            this._updateStatus(SOCKET_IO_DISCONNECTED)
          }
        })
      })
      .catch(({ response: { status, statusText, data } = {} }) => {
        this._updateStatus(SOCKET_IO_NO_CONNECTION)
        console.error(
          'Error while trying to authenticate, server responded:\n',
          {
            status,
            statusText,
            data
          }
        )
      })
  }
}

export default SocketIOWrapper
