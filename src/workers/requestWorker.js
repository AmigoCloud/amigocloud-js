import 'promise-polyfill/src/polyfill'

import axios from 'axios'
import qs from 'qs'
import axiosRetry from 'axios-retry'
// import Raven from 'raven-js'

// Raven.config(process.env.RAVEN_FRONTEND_DSN).install()

const MAX_RETRIES = 1000
const retryCondition = error => {
  return (
    axiosRetry.isNetworkOrIdempotentRequestError(error) ||
    // in case we get a network timeout it should also retry
    error.code === 'ECONNABORTED'
  )
}

axiosRetry(axios, {
  retries: MAX_RETRIES,
  retryDelay: axiosRetry.exponentialDelay,
  retryCondition
})

const handleMessage = ({ data: requestData }) => {
  if (requestData.useForm) {
    const form = new FormData()
    for (var key in requestData.data) {
      // Check if filename is in requestData and the type of data is Blob, to be send
      if (requestData.filename && requestData.data[key] instanceof Blob) {
        form.append(key, requestData.data[key], requestData.filename)
      } else {
        form.append(key, requestData.data[key])
      }
    }

    requestData.data = form
  }

  axios({
    ...requestData,
    paramsSerializer: params =>
      qs.stringify(params, {
        arrayFormat: 'repeat'
      })
  })
    .then(({ data }) => {
      postMessage({
        id: requestData.id,
        data
      })
    })
    .catch(({ response }) => {
      const responseData =
        typeof response.data === 'object' ? response.data : {}
      postMessage({
        id: requestData.id,
        error: response
          ? {
            ...responseData,
            status: response.status
          }
          : {
            detail: 'No Internet Connection',
            status: 0
          }
      })
    })
}

self.addEventListener('message', handleMessage)
